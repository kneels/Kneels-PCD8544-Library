PCD8544 Library
========
This is (yet another) library for the Philips PCD8544 (Nokia 3310) display. It's an extension of the basic library provided on [Arduino Playground](http://playground.arduino.cc/Code/PCD8544). 

The display mapping of the PCD8544 works as follows: There are 84x48 pixels availaible in total. The vertical pixels are divided into 6 addressable banks, so that one bank consists of 8 pixels vertically and 84 horizontally. Data written to the display is done in byte sized chunks that each represent one column in a bank. I needed to be able to address pixels individually rather than entire colomns at a time, so I added such functionality.