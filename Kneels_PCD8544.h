#define PIN_RST 12
#define PIN_CE 11
#define PIN_DC 10
#define PIN_DIN 9
#define PIN_CLK 8

#define LCD_X 84
#define LCD_Y 48
#define LCD_BANKS 6

#define EMPTY_COL 0x00

#define M_CMD LOW       // command mode
#define M_DATA HIGH     // data mode

/*  In order to address individual pixels, every byte per bank must be stored */
byte pixelGrid[LCD_X][LCD_BANKS]; 

static const byte ASCII[][5] =
{
 {0x00, 0x00, 0x00, 0x00, 0x00} // 20  
,{0x00, 0x00, 0x5f, 0x00, 0x00} // 21 !
,{0x00, 0x07, 0x00, 0x07, 0x00} // 22 "
,{0x14, 0x7f, 0x14, 0x7f, 0x14} // 23 #
,{0x24, 0x2a, 0x7f, 0x2a, 0x12} // 24 $
,{0x23, 0x13, 0x08, 0x64, 0x62} // 25 %
,{0x36, 0x49, 0x55, 0x22, 0x50} // 26 &
,{0x00, 0x05, 0x03, 0x00, 0x00} // 27 '
,{0x00, 0x1c, 0x22, 0x41, 0x00} // 28 (
,{0x00, 0x41, 0x22, 0x1c, 0x00} // 29 )
,{0x14, 0x08, 0x3e, 0x08, 0x14} // 2a *
,{0x08, 0x08, 0x3e, 0x08, 0x08} // 2b +
,{0x00, 0x50, 0x30, 0x00, 0x00} // 2c ,
,{0x08, 0x08, 0x08, 0x08, 0x08} // 2d -
,{0x00, 0x60, 0x60, 0x00, 0x00} // 2e .
,{0x20, 0x10, 0x08, 0x04, 0x02} // 2f /
,{0x3e, 0x51, 0x49, 0x45, 0x3e} // 30 0
,{0x00, 0x42, 0x7f, 0x40, 0x00} // 31 1
,{0x42, 0x61, 0x51, 0x49, 0x46} // 32 2
,{0x21, 0x41, 0x45, 0x4b, 0x31} // 33 3
,{0x18, 0x14, 0x12, 0x7f, 0x10} // 34 4
,{0x27, 0x45, 0x45, 0x45, 0x39} // 35 5
,{0x3c, 0x4a, 0x49, 0x49, 0x30} // 36 6
,{0x01, 0x71, 0x09, 0x05, 0x03} // 37 7
,{0x36, 0x49, 0x49, 0x49, 0x36} // 38 8
,{0x06, 0x49, 0x49, 0x29, 0x1e} // 39 9
,{0x00, 0x36, 0x36, 0x00, 0x00} // 3a :
,{0x00, 0x56, 0x36, 0x00, 0x00} // 3b ;
,{0x08, 0x14, 0x22, 0x41, 0x00} // 3c <
,{0x14, 0x14, 0x14, 0x14, 0x14} // 3d =
,{0x00, 0x41, 0x22, 0x14, 0x08} // 3e >
,{0x02, 0x01, 0x51, 0x09, 0x06} // 3f ?
,{0x32, 0x49, 0x79, 0x41, 0x3e} // 40 @
,{0x7e, 0x11, 0x11, 0x11, 0x7e} // 41 A
,{0x7f, 0x49, 0x49, 0x49, 0x36} // 42 B
,{0x3e, 0x41, 0x41, 0x41, 0x22} // 43 C
,{0x7f, 0x41, 0x41, 0x22, 0x1c} // 44 D
,{0x7f, 0x49, 0x49, 0x49, 0x41} // 45 E
,{0x7f, 0x09, 0x09, 0x09, 0x01} // 46 F
,{0x3e, 0x41, 0x49, 0x49, 0x7a} // 47 G
,{0x7f, 0x08, 0x08, 0x08, 0x7f} // 48 H
,{0x00, 0x41, 0x7f, 0x41, 0x00} // 49 I
,{0x20, 0x40, 0x41, 0x3f, 0x01} // 4a J
,{0x7f, 0x08, 0x14, 0x22, 0x41} // 4b K
,{0x7f, 0x40, 0x40, 0x40, 0x40} // 4c L
,{0x7f, 0x02, 0x0c, 0x02, 0x7f} // 4d M
,{0x7f, 0x04, 0x08, 0x10, 0x7f} // 4e N
,{0x3e, 0x41, 0x41, 0x41, 0x3e} // 4f O
,{0x7f, 0x09, 0x09, 0x09, 0x06} // 50 P
,{0x3e, 0x41, 0x51, 0x21, 0x5e} // 51 Q
,{0x7f, 0x09, 0x19, 0x29, 0x46} // 52 R
,{0x46, 0x49, 0x49, 0x49, 0x31} // 53 S
,{0x01, 0x01, 0x7f, 0x01, 0x01} // 54 T
,{0x3f, 0x40, 0x40, 0x40, 0x3f} // 55 U
,{0x1f, 0x20, 0x40, 0x20, 0x1f} // 56 V
,{0x3f, 0x40, 0x38, 0x40, 0x3f} // 57 W
,{0x63, 0x14, 0x08, 0x14, 0x63} // 58 X
,{0x07, 0x08, 0x70, 0x08, 0x07} // 59 Y
,{0x61, 0x51, 0x49, 0x45, 0x43} // 5a Z
,{0x00, 0x7f, 0x41, 0x41, 0x00} // 5b [
,{0x02, 0x04, 0x08, 0x10, 0x20} // 5c ¥
,{0x00, 0x41, 0x41, 0x7f, 0x00} // 5d ]
,{0x04, 0x02, 0x01, 0x02, 0x04} // 5e ^
,{0x40, 0x40, 0x40, 0x40, 0x40} // 5f _
,{0x00, 0x01, 0x02, 0x04, 0x00} // 60 `
,{0x20, 0x54, 0x54, 0x54, 0x78} // 61 a
,{0x7f, 0x48, 0x44, 0x44, 0x38} // 62 b
,{0x38, 0x44, 0x44, 0x44, 0x20} // 63 c
,{0x38, 0x44, 0x44, 0x48, 0x7f} // 64 d
,{0x38, 0x54, 0x54, 0x54, 0x18} // 65 e
,{0x08, 0x7e, 0x09, 0x01, 0x02} // 66 f
,{0x0c, 0x52, 0x52, 0x52, 0x3e} // 67 g
,{0x7f, 0x08, 0x04, 0x04, 0x78} // 68 h
,{0x00, 0x44, 0x7d, 0x40, 0x00} // 69 i
,{0x20, 0x40, 0x44, 0x3d, 0x00} // 6a j 
,{0x7f, 0x10, 0x28, 0x44, 0x00} // 6b k
,{0x00, 0x41, 0x7f, 0x40, 0x00} // 6c l
,{0x7c, 0x04, 0x18, 0x04, 0x78} // 6d m
,{0x7c, 0x08, 0x04, 0x04, 0x78} // 6e n
,{0x38, 0x44, 0x44, 0x44, 0x38} // 6f o
,{0x7c, 0x14, 0x14, 0x14, 0x08} // 70 p
,{0x08, 0x14, 0x14, 0x18, 0x7c} // 71 q
,{0x7c, 0x08, 0x04, 0x04, 0x08} // 72 r
,{0x48, 0x54, 0x54, 0x54, 0x20} // 73 s
,{0x04, 0x3f, 0x44, 0x40, 0x20} // 74 t
,{0x3c, 0x40, 0x40, 0x20, 0x7c} // 75 u
,{0x1c, 0x20, 0x40, 0x20, 0x1c} // 76 v
,{0x3c, 0x40, 0x30, 0x40, 0x3c} // 77 w
,{0x44, 0x28, 0x10, 0x28, 0x44} // 78 x
,{0x0c, 0x50, 0x50, 0x50, 0x3c} // 79 y
,{0x44, 0x64, 0x54, 0x4c, 0x44} // 7a z
,{0x00, 0x08, 0x36, 0x41, 0x00} // 7b {
,{0x00, 0x00, 0x7f, 0x00, 0x00} // 7c |
,{0x00, 0x41, 0x36, 0x08, 0x00} // 7d }
,{0x10, 0x08, 0x08, 0x10, 0x08} // 7e ←
,{0x78, 0x46, 0x41, 0x46, 0x78} // 7f →
};

void lcdWrite(byte dc, byte data) {
  digitalWrite(PIN_DC, dc);   // if dc set to low, data is interpreted as a command
  digitalWrite(PIN_CE, LOW);
  shiftOut(PIN_DIN, PIN_CLK, MSBFIRST, data);
  digitalWrite(PIN_CE, HIGH);
}

/** Moves the cursor to location x, bank y
* x - range: 0 to 84
* y - range: 0 to 5
*/
void gotoXY(int x, int y) {
  lcdWrite( M_CMD, 0x80 | x);  // Column.
  lcdWrite( M_CMD, 0x40 | y);  // Row.  
}

void lcdCharacter(char character) {
  lcdWrite(M_DATA, 0x00);
  for (int index = 0; index < 5; index++)
  {
    lcdWrite(M_DATA, ASCII[character - 0x20][index]);
  }
  lcdWrite(M_DATA, 0x00);
}

void lcdWriteString(char *characters) {
  while (*characters)
  {
    lcdCharacter(*characters++);
  }
}

void lcdClear(void) {
  for (int index = 0; index < LCD_X * LCD_Y / 8; index++)
  {
    lcdWrite(M_DATA, 0x00);
  }
}

void lcdInit(void) {
  pinMode(PIN_CE, OUTPUT);
  pinMode(PIN_RST, OUTPUT);
  pinMode(PIN_DC, OUTPUT);
  pinMode(PIN_DIN, OUTPUT);
  pinMode(PIN_CLK, OUTPUT);
  digitalWrite(PIN_RST, LOW);
  digitalWrite(PIN_RST, HIGH);
  lcdWrite(M_CMD, 0x21 );   // Use extended instruction set.
  lcdWrite(M_CMD, 0xB1 );   // Set LCD Vop (Contrast). 
  lcdWrite(M_CMD, 0x05 );   // Set Temp coefficent. //0x04
  lcdWrite(M_CMD, 0x14 );   // LCD bias mode 1:48. //0x13
  lcdWrite(M_CMD, 0x0C );   // Set display in normal mode.
  lcdWrite(M_CMD, 0x20 );   // Use basic instruction set.
  lcdWrite(M_CMD, 0x0C );   // Set display in normal mode.
  lcdClear();

  // initialize pixel grid with null bytes
  int i, j;
  for(i = LCD_X; i != 0; i--) {
    for(j = LCD_BANKS; j != 0; j--) {
      pixelGrid[i][j] = EMPTY_COL;
    }
  }
}


/** Turns a pixel on or off
* x: x-coordinate, 0-83
* y: y-coordinate, 0-47
* on: sets pixel on or off, 0-1 */
void setPixel(uint8_t x, uint8_t y, uint8_t on) {
  byte bitMask = 0x01 << y%8;
  uint8_t bank = y / 8;

  if(on) {
    pixelGrid[x][bank] |= bitMask;
  } else {
    pixelGrid[x][bank] ^= bitMask;
  }

  gotoXY(x, bank);
  lcdWrite(M_DATA, pixelGrid[x][bank]);
}

/** Draws a line 
* x: x-coordinate of the first point, 0-83
* y: y-coordinate of the first point, 0-47
* x: x-coordinate of the second point, 0-83
* y: y-coordinate of the second point, 0-47 */
void drawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2) {
  int i;
  double slope;
  
  if(x1 > x2) {
    uint8_t tmp;
    tmp = x1;
    x1 = x2;
    x2 = tmp;
    tmp = y1;
    y1 = y2;
    y2 = tmp;
  }

  slope = ((double) y2 - y1) / ((double) x2 - x1);
  
  for(i = x1; i <= x2; i++) {
    setPixel(i, i * slope, HIGH);
  }
}

/* Checks whether a pixel is set */
boolean checkPixel(uint8_t x, uint8_t y) {
  byte bitMask = 0x01 << y%8;
  uint8_t bank = y / 8;
  return pixelGrid[x][bank] & bitMask;
}
